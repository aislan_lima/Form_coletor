/*
SQLyog Community v12.4.1 (64 bit)
MySQL - 10.1.26-MariaDB : Database - db_teste
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_teste` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_teste`;

/*Table structure for table `tbl_coletor` */

DROP TABLE IF EXISTS `tbl_coletor`;

CREATE TABLE `tbl_coletor` (
  `colt_nome` varchar(40) DEFAULT NULL,
  `colt_sobrenome` varchar(80) DEFAULT NULL,
  `colt_email` varchar(80) DEFAULT NULL,
  `colt_celular` varchar(20) DEFAULT NULL,
  `colt_whats` varchar(5) DEFAULT NULL,
  `colt_internet` varchar(5) DEFAULT NULL,
  `colt_emp_coop` varchar(5) DEFAULT NULL,
  `colt_nome_emp` varchar(40) DEFAULT NULL,
  `colt_meio_trabalho` varchar(200) DEFAULT NULL,
  `colt_dias` varchar(500) DEFAULT NULL,
  `colt_periodo` varchar(250) DEFAULT NULL,
  `colt_materiais` varchar(500) DEFAULT NULL,
  `colt_cep` varchar(20) DEFAULT NULL,
  `colt_cidade` varchar(50) DEFAULT NULL,
  `colt_bairro` varchar(50) DEFAULT NULL,
  `colt_rua` varchar(80) DEFAULT NULL,
  `colt_num_casa` varchar(5) DEFAULT NULL,
  `colt_horario_inicial` varchar(10) DEFAULT NULL,
  `colt_horario_final` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_coletor` */

insert  into `tbl_coletor`(`colt_nome`,`colt_sobrenome`,`colt_email`,`colt_celular`,`colt_whats`,`colt_internet`,`colt_emp_coop`,`colt_nome_emp`,`colt_meio_trabalho`,`colt_dias`,`colt_periodo`,`colt_materiais`,`colt_cep`,`colt_cidade`,`colt_bairro`,`colt_rua`,`colt_num_casa`,`colt_horario_inicial`,`colt_horario_final`) values 
(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
