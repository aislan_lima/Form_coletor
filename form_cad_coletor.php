
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">

  <link rel="stylesheet" type="text/css" href="boot/css/bootstrap.min.css">
  <title> Cadastro de Coletor Parceiro</title>
</head>
<body>



  <div class="container">

    <form class="well form-horizontal" action="exe_colt.php" method="post"  id="contact_form">
      <fieldset>

        <legend>Cadastro de Coletores</legend>

        <!-- Nome -->

        <h4> Dados Pessoais </h4>

        <div class="form-group">
          <label class="col-md-4 control-label">Nome</label>  
          <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
          <input  name="nome" placeholder="Insira seu Nome" class="form-control"  type="text">
            </div>
          </div>
        </div>

        <!-- Sobrenome -->

        <div class="form-group">
          <label class="col-md-4 control-label" >Sobrenome</label> 
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
          <input name="sobrenome" placeholder="Insira seu Sobrenome" class="form-control"  type="text">
            </div>
          </div>
        </div>

        <!-- Email -->
               <div class="form-group">
          <label class="col-md-4 control-label">E-Mail</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
          <input name="email" placeholder="Insira seu E-mail" class="form-control"  type="text">
            </div>
          </div>
        </div>


        <!-- CELULAR -->
               
        <div class="form-group">
          <label class="col-md-4 control-label">Celular</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
          <input name="celular" placeholder="(12) 9 9999 - 9999" class="form-control" type="text">
            </div>
          </div>
        </div>

        <!-- ESTA LINHA TEM WHATSAPP-->
       <div class="form-group">
          <label class="col-md-4 control-label">Esta linha tem WhatsApp?</label>
          <div class="col-md-4">
            <div class="radio">
              <label>
                <input type="radio" name="whatsapp" value="sim" checked="checked" /> Sim
              </label>
              <label>
                <input type="radio" name="whatsapp" value="nao" /> Não
              </label>
            </div>
          </div>
        </div>

        <!-- VOCÊ TEM SMARTPHONE COM INTERNET-->
        <div class="form-group">
          <label class="col-md-4 control-label">Você tem internet em seu celular?</label>
          <div class="col-md-4">
            <div class="radio">
              <label>
                <input type="radio" name="internet" value="sim" checked="checked" /> Sim
              </label>
              <label>
                <input type="radio" name="internet" value="nao" /> Não
              </label>
            </div>
          </div>
        </div>

        <hr>
        <h4>Dados Profissionais</h4>

        <!-- PARTICIPA DE EMPRESA OU COOPERATIVA -->
        <div class="form-group">
          <label class="col-md-4 control-label">Participa de empresa ou cooperativa?</label>
          <div class="col-md-4">
            <div class="radio">
              <label>
                <input type="radio" name="emp_coop" value="sim" checked="checked" /> Sim
              </label>
              <label>
                <input type="radio" name="emp_coop" value="nao" /> Não
              </label>
            </div>
          </div>
        </div>

        <!-- NOME DA EMPRESA -->
        <div class="form-group">
          <label class="col-md-4 control-label">Nome da empresa</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
          <input name="nome_empresa" placeholder="Insira o nome da empesa/cooperativa" class="form-control" type="text">
            </div>
          </div>
        </div>

        <!-- MEIOS DE TRABALHO -->
        <div class="form-group">
          <label class="col-md-4 control-label">Qual seu meio de trabalho?</label>
          <div class="col-md-4">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="meios_trabalho[]" value="Manual" /> Manual(carroça)
              </label>
              <label>
                <input type="checkbox" name="meios_trabalho[]" value="Utilitario" /> Utilitário
              </label>
              <label>
                <input type="checkbox" name="meios_trabalho[]" value="Caminhao" /> Caminhão
              </label>
            </div>
          </div>
        </div>
      

        <!-- DIAS DA SEMANA -->
        <div class="form-group">
          <label class="col-md-4 control-label">Dias em que você coleta</label>
          <div class="col-md-4">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="dias[]" value="segunda" /> Seguda-feira
              </label>
              <label>
                <input type="checkbox" name="dias[]" value="terca" /> Terça-feira
              </label>
              <label>
                <input type="checkbox" name="dias[]" value="quarta" /> Quarta-feira
              </label>
               <label>
                <input type="checkbox" name="dias[]" value="quinta" /> Quinta-feira
              </label>
               <label>
                <input type="checkbox" name="dias[]" value="sexta" /> Sexta-feira
              </label>
               <label>
                <input type="checkbox" name="dias[]" value="sabado" /> Sábado
              </label>
               <label>
                <input type="checkbox" name="dias[]" value="domingo" /> Domingo
              </label>
            </div>
          </div>
        </div>

        <!-- PERÍODO -->
        <div class="form-group">
          <label class="col-md-4 control-label">Período de trabalho:</label>
          <div class="col-md-4">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="periodo[]" value="manha" />Manhã
              </label>
              <label>
                <input type="checkbox" name="periodo[]" value="tarde" /> Tarde
              </label>
              <label>
                <input type="checkbox" name="periodo[]" value="integral" /> Integral(horário comercial)
              </label>
            </div>
          </div>
        </div>

        <!-- HORÁRIO DA JORNADA -->
        <div class="form-group"> 
          <label class="col-md-4 control-label">Horário aproximado da jornada</label>
          <div class="col-md-4 selectContainer">
            <div class="input-group">
              <select name="horario_inicial" class="form-control selectpicker" >
                <option>Início de trabalho</option>
                <option value="06:00">06:00</option>
                <option value="06:30">06:30</option>
                <option value="07:00">07:00</option>
                <option value="07:30">07:30</option>
                <option value="08:00">08:00</option>
                <option value="08:30">08:30</option>
                <option value="09:00">09:00</option>
                <option value="09:30">09:30</option>
                <option value="10:00">10:00</option>
                <option value="10:30">10:30</option>
                <option value="11:00">11:00</option>
                <option value="11:30">11:30</option>
                <option value="12:00">12:00</option>
                <option value="12:30">12:30</option>
                <option value="13:00">13:00</option>
                <option value="13:30">13:30</option>
                <option value="14:00">14:00</option>
                <option value="14:30">14:30</option>
                <option value="15:00">15:00</option>
                <option value="15:30">15:30</option>
                <option value="16:00">16:00</option>
                <option value="16:30">16:30</option>
                <option value="17:00">17:00</option>
                <option value="17:30">17:30</option>
                <option value="18:00">18:00</option>
                <option value="18:30">18:30</option>

              </select>
            </div>
          </div>
          <div class="col-md-4 col-md-offset-4 selectContainer">
            <div class="input-group">
              <select name="horario_final" class="form-control selectpicker" >
                <option>Término de trabalho</option>
                <option value="06:00">06:00</option>
                <option value="06:30">06:30</option>
                <option value="07:00">07:00</option>
                <option value="07:30">07:30</option>
                <option value="08:00">08:00</option>
                <option value="08:30">08:30</option>
                <option value="09:00">09:00</option>
                <option value="09:30">09:30</option>
                <option value="10:00">10:00</option>
                <option value="10:30">10:30</option>
                <option value="11:00">11:00</option>
                <option value="11:30">11:30</option>
                <option value="12:00">12:00</option>
                <option value="12:30">12:30</option>
                <option value="13:00">13:00</option>
                <option value="13:30">13:30</option>
                <option value="14:00">14:00</option>
                <option value="14:30">14:30</option>
                <option value="15:00">15:00</option>
                <option value="15:30">15:30</option>
                <option value="16:00">16:00</option>
                <option value="16:30">16:30</option>
                <option value="17:00">17:00</option>
                <option value="17:30">17:30</option>
                <option value="18:00">18:00</option>
                <option value="18:30">18:30</option>

              </select>
            </div>
          </div>
        </div>


        <!-- MATERIAIS -->
        <div class="form-group">
          <label class="col-md-4 control-label">Materiais com o qual trabalha:</label>
          <div class="col-md-4">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="materiais[]" value="papelao" />Papelão
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="papel_branco" /> Papel branco 
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="latas_aluminio" /> Latas de alumínio
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="latas_aco" /> Latas de aço
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="vidros" /> Vidros
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="plastico_rigido" /> Plástico rígido
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="plastico_filme" /> Plástico filme
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="longa_vida" /> Longa vida
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="oleo" /> Óleo
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="metal" /> Metal
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="eletronicos" /> Eletrônicos
              </label>
              <label>
                <input type="checkbox" name="materiais[]" value="pet" /> PET
              </label>
            </div>
          </div>
        </div>



        <hr>
        <h4> Endereço </h4>

        <!-- CEP -->
              
        <div class="form-group">
          <label class="col-md-4 control-label">CEP</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
          <input name="cep" placeholder="CEP" class="form-control" id="cep"  type="text">
            </div>
          </div>
        </div>

        <!-- CIDADE-->
         
        <div class="form-group">
          <label class="col-md-4 control-label">Cidade</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
          <input name="cidade" placeholder="Cidade" class="form-control" id="cidade"  type="text">
            </div>
          </div>
        </div>

        <!-- BAIRRO -->
        <div class="form-group">
          <label class="col-md-4 control-label">Bairro</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
          <input name="bairro" placeholder="Bairro" class="form-control" id="bairro"  type="text">
            </div>
          </div>
        </div>

         <!-- RUA -->
        <div class="form-group">
          <label class="col-md-4 control-label">Rua</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
          <input name="rua" placeholder="Rua" class="form-control" id="rua"  type="text">
            </div>
          </div>
        </div>

        <!-- NÚMERO DA CASA -->
         <div class="form-group">
          <label class="col-md-4 control-label">Númeor da casa</label>  
            <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
          <input name="num_casa" placeholder="Casa" class="form-control"  type="text">
            </div>
          </div>
        </div>

        <hr>


        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label"></label>
          <div class="col-md-4">
            <button type="submit" class="btn btn-success" >Enviar <span class="glyphicon glyphicon-send"></span></button>
          </div>
        </div>

      </fieldset>
    </form>
  </div><!-- /.container -->

  <script type="text/javascript" href="boot/js/main.js"></script>
  <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
  
</body>
</html>
